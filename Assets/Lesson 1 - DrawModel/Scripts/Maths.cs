using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Raytrace.Math
{
	public struct RayHit
	{
		public Vector3 point;
		public Vector3 normal;
		public float distance;
		public object rendergeometry;

		public static implicit operator RayHit(RaycastHit info)
		{
			return new RayHit
			{
				point = info.point,
				normal = info.normal,
				distance = info.distance,
			};
		}
	}

	public static class Maths
    {
		/// <summary>
		/// Detect if ray are intersect the target Sphere.
		/// <see cref="https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-sphere-intersection"/>
		/// </summary>
		/// <param name="ray"></param>
		/// <param name="origin">sphere origin</param>
		/// <param name="radius">sphere radius</param>
		/// <param name="rayHit">return result</param>
		/// <param name="detectInsideSphere">Assign true = when <paramref name="ray"/> are start inside the sphere, also define hitted.</param>
		/// <returns>hit = true</returns>
		public static bool IntersectRaySphere(in Ray ray, Vector3 origin, float radius, float maxDistance, ref RayHit rayHit,
			bool detectInsideSphere = false)
		{
			float t0 = 0f, t1 = 0f; // solutions for "t" if the ray intersects;
			float radius2 = radius * radius;
			Vector3 rayDirNormalized = ray.direction.normalized;
#if GEOMETRIC_SOLUTION || true
			// Geometric solution
			Vector3 L = origin - ray.origin; // Notes : L := known as the long side of triangle

			float tca = Vector3.Dot(L, rayDirNormalized); // Notes: tca := L project into ray.direction
			if (tca < 0f)
				return false; // inverse direction.

			float d2 = Vector3.Dot(L, L) - tca * tca; // d := based on Pythagorean theorem, L^2 - tca^2 = d^2
			if (d2 > radius2)
				return false;
			// Notes : thc := the distance between sphere center & intersect points
			float thc = Mathf.Sqrt(radius2 - d2);
			t0 = tca - thc;
			t1 = tca + thc;
			if (t0 > t1)
			{
				var tmp = t0;
				t0 = t1;
				t1 = tmp;
			}
#else
			// #elif ANALYTIC_SOLUTION //|| true
			// Analytic solution
			Vector3 L = ray.origin - origin;
			float a = Vector3.Dot(ray.direction, ray.direction); // a = ray.magnitude^2
			float b = 2f * Vector3.Dot(L, ray.direction);
			float c = Vector3.Dot(L, L) - radius2;
			// Notes: since intersect point - origin should always equal to radius.
			// : because distance(t) should always land on (L) vector
			// intersect point === ray.origin + ray.direction * distance(t) = L
			// L.Dot(L) = L.magnitude^2 = "intersect point to origin distance"^2 - radius^2 = 0
			if (!SolveQuadratic(a, b, c, ref t0, ref t1))
				return false;
#endif
			if (t0 > maxDistance)
				return false;
			if (t0 < 0f) // If t0 is negative, ray started inside sphere
			{
				if (detectInsideSphere)
				{
					t0 = t1; // started inside sphere, let's use t1 instead
					if (t0 < 0f)
						return false; // both t0 & t1 are negative, behind ray
				}
				else
				{
					// ray started inside sphere.
					return false;
				}
			}

			Vector3 point = ray.origin + (ray.direction.normalized * t0);
			Vector3 normal = (point - origin).normalized;
			rayHit = new RayHit
			{
				point = point,
				normal = normal,
				distance = t0,
			};
			return true;
		}

		/// <summary>
		/// To detect if giving ray can interact the giving mesh model.
		/// </summary>
		/// <param name="ray"></param>
		/// <param name="maxDistance"></param>
		/// <param name="mesh"></param>
		/// <param name="rayHit"></param>
		/// <param name="matrix"></param>
		/// <returns></returns>
		public static bool IntersectPointOnMesh(in Ray ray, float maxDistance, Mesh mesh, ref RayHit rayHit, Matrix4x4 matrix = default)
		{
			if (matrix == default)
				matrix = Matrix4x4.identity;
			Vector3[] vertices = mesh.vertices;
			bool alreadyHit = false;
			for (int submesh = 0; submesh < mesh.subMeshCount; submesh++)
			{
				int triCnt = (int)mesh.GetIndexCount(submesh) / 3;
				int[] indices = mesh.GetIndices(submesh);
				uint subMeshVertexStart = mesh.GetBaseVertex(submesh);
				for (int tri = 0; tri < triCnt; tri++)
				{
					int i = tri * 3;
					Vector3
						p0 = matrix.MultiplyPoint(vertices[subMeshVertexStart + indices[i]]),
						p1 = matrix.MultiplyPoint(vertices[subMeshVertexStart + indices[i + 1]]),
						p2 = matrix.MultiplyPoint(vertices[subMeshVertexStart + indices[i + 2]]);
					if (IntersectPointOnTriangle(ray, maxDistance, p0, p1, p2, ref rayHit))
					{
						maxDistance = rayHit.distance;
						alreadyHit = true;
					}
				}
			}
			return alreadyHit;
		}

		/// <summary>
		/// <see cref="https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/geometry-of-a-triangle"/>
		/// </summary>
		/// <param name="ray"></param>
		/// <param name="maxDistance"></param>
		/// <param name="p0"></param>
		/// <param name="p1"></param>
		/// <param name="p2"></param>
		/// <param name="rayHit"></param>
		/// <returns></returns>
		public static bool IntersectPointOnTriangle(in Ray ray, float maxDistance, in Vector3 p0, in Vector3 p1, in Vector3 p2, ref RayHit rayHit)
		{
			// Triangle side, p0>p1>p2, assume clockwise order
			Vector3
				edge01 = p0 - p1,
				edge12 = p1 - p2,
				edge20 = p2 - p0;
			Vector3 surfaceNormal = Vector3.Cross(edge20, edge01).normalized;

			RayHit hit = default;
			if (!IntersectPointOnPlane(ray, maxDistance, p0, surfaceNormal, ref hit))
				return false;

			bool IsOutsideEdge(in Vector3 edge, in Vector3 point2hitPoint, in Vector3 surfaceNormal)
			{
				// compare (edge vs point2hitPoint) vector & compareEdge
				Vector3 v = Vector3.Cross(edge, point2hitPoint);
				// is in front or behind the edge. assume pointing forward is outside.
				return Vector3.Dot(surfaceNormal, v) < 0f;
			}
			if (IsOutsideEdge(edge01, p0 - hit.point, surfaceNormal) ||
				IsOutsideEdge(edge12, p1 - hit.point, surfaceNormal) ||
				IsOutsideEdge(edge20, p2 - hit.point, surfaceNormal))
				return false;

			/// assume <see cref="IsOutsideEdge(in Vector3, in Vector3, in Vector3, in Vector3)"/> correct
			/// now we are within the triangle.
			rayHit.point = hit.point;
			rayHit.normal = surfaceNormal;
			rayHit.distance = hit.distance;
			return true;
		}

		/// <summary>Giving point and ray to calculate the intersect point on target surface normal.
		/// <see cref="https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection"/>
		/// <see cref="https://www.youtube.com/watch?v=JKTHQvkXmnY&list=PLeGk08zVu454f47YNid_bRTGbo9_kIU5c&index=2"/>
		/// </summary>
		/// <param name="ray">unit vector is required</param>
		/// <param name="p0">any point on plane, used for triangle calculation</param>
		/// <param name="surfaceNormal">normal vector of plane</param>
		/// <param name="rayHit">return ray hit detail.</param>
		/// <returns>Hit = true</returns>
		public static bool IntersectPointOnPlane(this in Ray ray, float maxDistance, in Vector3 p0, Vector3 surfaceNormal, ref RayHit rayHit)
		{
#if LONG_VERSION
			float denominator = Vector3.Dot(surfaceNormal, ray.direction);
			if (denominator.Approximately(0f))
				return false; // parallel to the plane;
			if (denominator > 0f)
				return false; // cull back face.
			float planeDistance = Vector3.Dot(surfaceNormal, p0);
			float distance = (planeDistance - Vector3.Dot(ray.origin, surfaceNormal)) / denominator;
			if (0f < distance && distance <= maxDistance)
			{
				rayHit.point = ray.origin + (ray.direction * distance);
				rayHit.normal = surfaceNormal;
				rayHit.distance = distance;
				return true;
			}
#else
			float denominator = -Vector3.Dot(surfaceNormal, ray.direction); // reverse to intersect
			if (denominator > 1e-6) // avoid too close to 0, float point error.
			{
				float distance = Vector3.Dot(surfaceNormal, ray.origin - p0) / denominator;
				if (0f < distance && distance <= maxDistance)
				{
					rayHit.point = ray.origin + (ray.direction * distance);
					rayHit.normal = surfaceNormal;
					rayHit.distance = distance;
					return true;
				}
			}
#endif
			return false;
		}
	}
}