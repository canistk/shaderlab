using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Raytrace
{
    public class MyMaterial : MonoBehaviour
    {
        public Renderer m_Renderer = null;
        public Color color => m_Renderer ? m_Renderer.sharedMaterial.color : Color.white;
        public float m_Reflection = 0f;
        // public float m_Refraction = 1.33f; // water

        private void OnValidate()
        {
            if (m_Renderer == null)
                m_Renderer = GetComponent<Renderer>();
        }
    }
}