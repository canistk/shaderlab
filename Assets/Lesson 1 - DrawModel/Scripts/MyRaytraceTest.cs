using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;
#if UNITY_EDITOR
using UnityEditor;
#endif
using DebugKit;
using Raytrace.Math;

namespace Raytrace
{
	public class RenderGeometry
	{
		public Collider m_Collider;
		public MyMaterial m_Material;
		public RenderGeometry(Collider collider, MyMaterial material)
		{
			m_Collider = collider;
			m_Material = material;
		}
	}

	[ExecuteInEditMode]
	public class MyRaytraceTest : MonoBehaviour
	{
		#region EDITOR
#if UNITY_EDITOR
		[MenuItem("MyRenderer/Render %F1")] static void MenuItem_Render() => s_instance?.CPURenderPipeline();
		// [MenuItem("MyRenderer/Random Color %F2")] static void Random_Color() => s_instance.m_IntersectColor = new Color(Random.value, Random.value, Random.value);
#endif

		MyRaytraceTest()
		{
			s_instance = this;
			m_Stopwatch = new Stopwatch();
		}
		static MyRaytraceTest s_instance;
		#endregion EDITOR

		#region Variable
		public Camera m_Camera = null;
		public Light m_light = null;

		[Header("Colors")]
		public Color m_BackgroundColor = Color.black;


		[Header("Method(s)")]
		[SerializeField] eScreenRay m_ScreenRay = eScreenRay.ByProjectionMatrix;
		private enum eScreenRay
		{
			ByImagePlane,
			ByProjectionMatrix,
		}
		[SerializeField] eIntersect m_Intersect = eIntersect.Physic;
		private enum eIntersect
		{
			Physic,
			CustomCalculation,
		}
		[SerializeField] int m_MaxIteration = 3;

		[Header("Debug")]
		public bool m_DebugCamera = false;
		public bool m_DebugReflection = false;
		public Vector2 m_DebugLabelOffset = Vector2.zero;
		[Range(-1f, 1f)] public float m_DebugNearZSample = -1f;
		[Range(-1f, 1f)] public float m_DebugFarZSample = 1f;
		[Space]
		[Range(0f, 1f)] public float m_DebugRayX = 0.5f;
		[Range(0f, 1f)] public float m_DebugRayY = 0.5f;
		private Stopwatch m_Stopwatch;
		#endregion Variable

		#region Texture
		public Texture2D m_Texture = null; // debug purpose.
		private Texture2D texture
		{
			get
			{
				// Auto re-init when texture didn't match screen.
				if (m_Texture == null ||
					m_Texture.width != ScreenWidth ||
					m_Texture.height != ScreenHeight)
					m_Texture = new Texture2D(ScreenWidth, ScreenHeight);
				return m_Texture;
			}
		}
		public int ScreenWidth => Screen.width;
		public int ScreenHeight => Screen.height;
		#endregion Texture


		private void OnGUI()
		{
			if (m_Texture)
			{
				GUI.DrawTexture(new Rect(0, 0, m_Texture.width, m_Texture.height), m_Texture);
			}
			else
			{
				GUILayout.Box("======== Render by Ctrl+F1 =======");
			}
		}

		private void OnDrawGizmos()
		{
			if (m_Camera &&
				(m_DebugCamera || m_DebugReflection))
			{
				CameraData camera = new CameraData(m_Camera);
				int x = Mathf.RoundToInt(m_DebugRayX * ScreenWidth);
				int y = Mathf.RoundToInt(m_DebugRayY * ScreenHeight);
				Vector2 clip = ScreenToClipCoordinate(x, y);
				List<RenderGeometry> renderGeometries = GetRenderGeometryInScene();
				Ray ray = ScreenPointToRay(camera, clip, m_DebugCamera);
				if (m_DebugCamera)
					GizmosExtend.DrawRay(camera.position, ray.direction, Color.magenta);
				Color geometryColor = Color.clear;

				if (!DrawGeometry(ray, camera.farClipPlane, myLightDir.normalized,
					renderGeometries, m_MaxIteration,
					m_DebugReflection,
					ref geometryColor, out RenderGeometry hitGeometry) &&
					m_DebugReflection)
				{
					GizmosExtend.DrawRay(ray.origin, ray.direction * 100f, Color.gray);
				}
				if (m_DebugCamera)
					GizmosExtend.DrawLabel(camera.position,
						$"Screen w={ScreenWidth},h={ScreenHeight}\n" +
						$"Pixel[{x}][{y}]\n" +
						$"Clip[{clip.x:F2}][{clip.y:F2}]\n", m_DebugLabelOffset.x, m_DebugLabelOffset.y);
			}
		}

		private void ClearTexture()
		{
			Color[] pixels = new Color[texture.width * texture.height];
			for (int i = 0; i < pixels.Length; i++)
			{
				pixels[i] = m_BackgroundColor;
			}
			texture.SetPixels(pixels);
		}

		private void CPURenderPipeline()
		{
			ClearTexture();

			CameraData cameraData = new CameraData(m_Camera);

			List<RenderGeometry> renderGeometry = GetRenderGeometryInScene();
			// RemoveOutRangeCollider(colliders);

			DrawCustomCamera(cameraData, renderGeometry);

			texture.Apply(false);
		}

		#region Calculate Camera matrix
		private struct CameraData
		{
			public Vector3 position;
			public Quaternion rotation;
			public float fieldOfView, aspect, nearClipPlane, farClipPlane;
			public Matrix4x4 projectionMatrix, localToWorldMatrix, worldToCameraMatrix, cameraToWorldMatrix;
			private Matrix4x4 cachedCameraProjection2WorldMatrix;
			private bool cached;
			public CameraData(Camera camRef)
			{
				position = camRef.transform.position;
				rotation = camRef.transform.rotation;
				fieldOfView = camRef.fieldOfView;
				aspect = camRef.aspect;
				nearClipPlane = camRef.nearClipPlane;
				farClipPlane = camRef.farClipPlane;
				localToWorldMatrix = camRef.transform.localToWorldMatrix;
				projectionMatrix = camRef.projectionMatrix;
				worldToCameraMatrix = camRef.worldToCameraMatrix;
				cameraToWorldMatrix = camRef.cameraToWorldMatrix;
				cachedCameraProjection2WorldMatrix = default;
				cached = false;
			}

			private struct ScreenCoordinates { public float top, bottom, left, right; }

			/// <summary>
			/// we got distance (near plane) & tan angle(Fov/2) calculate the vertial scale.
			/// 
			///	 ____ <- Vertial (center to top)
			///  |  /
			/// D| /
			///  |/
			/// </summary>
			/// <returns></returns>
			private ScreenCoordinates CalcScreenCoordinates()
			{
				// FOV
				float halfFov = fieldOfView * 0.5f * Mathf.Deg2Rad;
				float scale = Mathf.Tan(halfFov) * nearClipPlane;
				float right = aspect * scale; // aspect height : width.

				return new ScreenCoordinates
				{
					top = scale,
					bottom = -scale,
					right = right,
					left = -right,
				};
			}

			/// <summary>OpenGL perspective projection matrix, convert into unity space
			/// <see cref="https://www.scratchapixel.com/lessons/3d-basic-rendering/perspective-and-orthographic-projection-matrix/opengl-perspective-projection-matrix"/>
			/// </summary>
			/// <returns></returns>
			public Matrix4x4 GetInverseProjectionMatrix()
			{
				if (!cached)
				{
					ScreenCoordinates sc = CalcScreenCoordinates();
					Matrix4x4 mat = new Matrix4x4
					{
						m00 = 2f * nearClipPlane / (sc.right - sc.left),
						m10 = 0f,
						m20 = 0f,
						m30 = 0f,

						m01 = 0f,
						m11 = 2f * nearClipPlane / (sc.top - sc.bottom),
						m21 = 0f,
						m31 = 0f,

						m02 = (sc.right + sc.left) / (sc.right - sc.left),
						m12 = (sc.top + sc.bottom) / (sc.top - sc.bottom),
						m22 = -(farClipPlane + nearClipPlane) / (farClipPlane - nearClipPlane),
						m32 = -1f,

						m03 = 0f,
						m13 = 0f,
						m23 = -2f * farClipPlane * nearClipPlane / (farClipPlane - nearClipPlane),
						m33 = 0f,
					};

					/// Inverse & cache to reduce overhead in for-loop.
					mat = Matrix4x4.Inverse(mat);

					/// OpenGL convention, near plane = -1, far plane = 1
					/// Note : did the same thing as U3D did.
					/// <see cref="https://docs.unity3d.com/ScriptReference/Matrix4x4.Perspective.html"/>
					/// The returned matrix embeds a z-flip operation 
					/// whose purpose is to cancel the z-flip performed
					/// by the camera view matrix. If the view matrix is an identity
					/// or some custom matrix that doesn't perform a z-flip,
					/// consider multiplying the third column of the projection matrix
					/// (i.e. m02, m12, m22 and m32) by -1.
					mat.SetRow(2, mat.GetRow(2) * -1f);

					// Cached!
					cached = true;
					cachedCameraProjection2WorldMatrix = mat;
				}
				return cachedCameraProjection2WorldMatrix;
			}
		}

		private void DrawCustomCamera(in CameraData camera, List<RenderGeometry> renderGeometries)
		{
			Ray ray;
			Vector3 lightDir = myLightDir.normalized;
			m_Stopwatch.Restart();
			Color[] colors = texture.GetPixels();
			for (int y = 0; y < ScreenHeight; y++)
			{
				for (int x = 0; x < ScreenWidth; x++)
				{
					ray = ScreenPointToRay(camera, ScreenToClipCoordinate(x, y));
					Color exist = colors[y * ScreenWidth + x];
					Color geometryColor = exist;
					Color finalColor = exist;
					if (DrawGeometry(ray, camera.farClipPlane, lightDir,
						renderGeometries, m_MaxIteration,
						false,
						ref geometryColor, out RenderGeometry hitGeometry))
					{
						float weight = hitGeometry.m_Material.color.a;
						finalColor = Color.Lerp(exist, geometryColor, weight);
						finalColor.a = Mathf.Clamp01(exist.a + geometryColor.a);
					}
					colors[y * ScreenWidth + x] = finalColor;
				}
			}
			texture.SetPixels(colors);
			m_Stopwatch.Stop();
			Debug.Log($"{nameof(DrawCustomCamera)}, completed in {m_Stopwatch.ElapsedMilliseconds} ms");
		}

		private bool DrawGeometry(in Ray ray, float maxDistance,
			Vector3 lightDir,
			List<RenderGeometry> renderGeometries, int iteratior,
			bool drawDebug,
			ref Color geometryColor, out RenderGeometry hitGeometry)
		{
			hitGeometry = null;
			if (maxDistance < 0f)
				Debug.LogError("Distance must >= 0f");
			if (iteratior < 0 || !Intersect(ray, maxDistance, renderGeometries, out RayHit rayHit))
				return false;
			// Locate material on geometry.
			hitGeometry = rayHit.rendergeometry as RenderGeometry;
			if (hitGeometry == null || hitGeometry.m_Material == null)
				return false;

			geometryColor = hitGeometry.m_Material.color * Mathf.Clamp01(Vector3.Dot(rayHit.normal, -lightDir));
			geometryColor.a = hitGeometry.m_Material.color.a;
			Color nextGeometryColor = geometryColor;
			if (drawDebug)
			{
				DebugExtend.DrawArrow(ray.origin, ray.direction * rayHit.distance, geometryColor);
			}

			if (hitGeometry.m_Material.m_Reflection > 0f)
			{
				float reflectDot = Vector3.Dot(ray.direction, rayHit.normal);
				if (reflectDot < 0f)
				{
#if CUSTOM_REFLECTED || true
					// Vector3 projectedNormal = rayHit.normal * (-2f * Vector3.Dot(ray.direction, rayHit.normal));
					Vector3 projectedNormal = rayHit.normal * (-2f * reflectDot);
					Vector3 reflectedNormal = (ray.direction + projectedNormal).normalized;
					Ray reflectedRay = new Ray(rayHit.point, reflectedNormal);
					if (drawDebug)
					{
						DebugExtend.DrawRay(rayHit.point, projectedNormal * 0.5f, nextGeometryColor);
						DebugExtend.DrawCircle(rayHit.point, projectedNormal, nextGeometryColor, 0.1f);
					}
#else
				Ray reflectedRay = new Ray(rayHit.point, Vector3.Reflect(rayHit.normal, ray.direction).normalized);
#endif
					if (DrawGeometry(reflectedRay, float.PositiveInfinity, reflectedRay.direction, renderGeometries, iteratior - 1,
						drawDebug, ref nextGeometryColor, out RenderGeometry nextGeometry))
					{
						geometryColor = Color.Lerp(geometryColor, nextGeometryColor, hitGeometry.m_Material.m_Reflection);
					}
					else if (drawDebug)
					{
						DebugExtend.DrawRay(reflectedRay.origin, reflectedRay.direction * 100f, Color.gray);
					}
					geometryColor.a = hitGeometry.m_Material.color.a;
				}
			}

			return true;
		}

		private Vector3 myLightDir => m_light.transform.forward;// new Vector3(1f, -1f, 1f);

		/// <summary>Convert Screen pixel into Clip coordinate</summary>
		/// <param name="screenX">between 0 ~ Screen.Width</param>
		/// <param name="screenY">between 0 ~ Screen.Height</param>
		/// <returns>Vector2 between (-1,-1) ~ (1,1), center = (0,0)</returns>
		private Vector2 ScreenToClipCoordinate(int screenX, int screenY)
		{
			float px = (float)screenX / (float)ScreenWidth; // 0 ~ 1
			float py = (float)screenY / (float)ScreenHeight;
			return new Vector2(
				(px - 0.5f) * 2f, // -1 ~ 1
				(py - 0.5f) * 2f);
		}

		/// <summary>Wrapper function to switch testing method.</summary>
		private Ray ScreenPointToRay(in CameraData camera, Vector2 clip, bool debugDraw = false)
		{
			if (m_ScreenRay == eScreenRay.ByImagePlane)
				return ScreenPointToRay_ByImagePlaneDistance(camera, clip, debugDraw);
			else if (m_ScreenRay == eScreenRay.ByProjectionMatrix)
				return ScreenPointToRay_ByMatrix(camera, clip, debugDraw);
			throw new System.NotImplementedException();
		}

		/// <summary>
		/// <see cref="https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-837-computer-graphics-fall-2012/lecture-notes/MIT6_837F12_Lec11.pdf"/>
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		private Ray ScreenPointToRay_ByImagePlaneDistance(in CameraData camera, Vector2 clip, bool debugDraw = false)
		{
			/// Find the distance of "image plane" in front of camera,
			/// let the plane's width normalize -1 < x < 1, assume width/2f = 1 unit.
			/// so we can find Distance = 1 / tan(fov/2)
			/// <see cref="https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-837-computer-graphics-fall-2012/lecture-notes/MIT6_837F12_Lec11.pdf"/>
			// since : tan(fov/2f) = 1 / Distance
			float planeDistance = (1f / Mathf.Tan(camera.fieldOfView * Mathf.Deg2Rad / 2f));

			// r = p - e = (x * u, aspect * y*v, D * w)
			Vector3 DW = camera.rotation * (Vector3.forward * planeDistance);
			Vector3 XU = camera.rotation * (Vector3.right * (camera.aspect * clip.x));
			Vector3 YV = camera.rotation * (Vector3.up * clip.y);
			Vector3 e = camera.position;
			if (debugDraw)
			{
				Debug.DrawRay(camera.position, DW, Color.blue);
				Debug.DrawRay(camera.position + DW, XU, Color.red);
				Debug.DrawRay(camera.position + DW, YV, Color.green);
			}
			Vector3 r = (XU + YV + DW).normalized;
			return new Ray(e, r);
		}

		/// <summary>By using the projection matrix from camera,
		/// 
		/// </summary>
		/// <param name="camera"></param>
		/// <param name="clip"></param>
		/// <param name="drawDebug"></param>
		/// <returns></returns>
		private Ray ScreenPointToRay_ByMatrix(in CameraData camera, Vector2 clip, bool drawDebug = false)
		{
			// Note : Clip X,Y range within -1 ~ 1
#if UNITY_VERSION
		Matrix4x4 inverseProjection = camera.projectionMatrix.inverse;
#else
			Matrix4x4 inverseProjection = camera.GetInverseProjectionMatrix();
#endif

#if LONG_VERSION
		Vector3 nearPoint = new Vector3(clip.x, clip.y, m_DebugNearZSample);
		Vector3 farPoint = new Vector3(clip.x, clip.y, m_DebugFarZSample);

		Vector3 localNear = inverseProjection.MultiplyPoint(nearPoint);
		Vector3 localFar = inverseProjection.MultiplyPoint(farPoint);

		Vector3 worldNear = camera.localToWorldMatrix.MultiplyPoint(localNear);
		Vector3 worldFar = camera.localToWorldMatrix.MultiplyPoint(localFar);
#else
			Matrix4x4 combine = camera.localToWorldMatrix * inverseProjection;
			Vector3 worldNear = combine.MultiplyPoint(new Vector3(clip.x, clip.y, m_DebugNearZSample));
			Vector3 worldFar = combine.MultiplyPoint(new Vector3(clip.x, clip.y, m_DebugFarZSample));
#endif

			if (drawDebug)
			{
				DebugExtend.DrawLine(worldFar, worldNear, Color.yellow);
				DebugExtend.DrawWireSphere(worldNear, 0.05f, Color.yellow);
				DebugExtend.DrawWireSphere(worldFar, 0.05f, Color.blue);
			}
			return new Ray(worldNear, (worldFar - worldNear).normalized);
		}
		#endregion Calculate Camera matrix

		#region Collider

		private List<RenderGeometry> GetRenderGeometryInScene()
		{
			GameObject[] rootObjects = m_Camera.gameObject.scene.GetRootGameObjects();
			List<Collider> colliders = new List<Collider>(rootObjects.Length * 100);
			m_Stopwatch.Restart();
			foreach (GameObject root in rootObjects)
			{
				colliders.AddRange(root.GetComponentsInChildren<Collider>());
			}

			List<RenderGeometry> rst = new List<RenderGeometry>(colliders.Count);
			foreach (Collider collider in colliders)
			{
				if (collider.gameObject.activeInHierarchy)
					rst.Add(new RenderGeometry(collider, collider.GetComponent<MyMaterial>()));
			}

			m_Stopwatch.Stop();
			// Debug.Log($"Get All Collider[{colliders.Count}] in scene, {m_Stopwatch.ElapsedMilliseconds} ms");
			return rst;
		}

		private void RemoveOutRangeCollider(List<Collider> colliders)
		{
			m_Stopwatch.Restart();
			colliders.RemoveAll(FilterOutRangeCollider);
			m_Stopwatch.Stop();
			Debug.Log($"{nameof(RemoveOutRangeCollider)}, completed in {m_Stopwatch.ElapsedMilliseconds} ms");
		}

		private bool FilterOutRangeCollider(Collider collider)
		{
			Vector3 cameraForward = m_Camera.transform.forward;
			Vector3 cameraPos = m_Camera.transform.position;
			Vector3 objPos = collider.transform.position;
			Vector3 dir = objPos - cameraPos;
			float dot = Vector3.Dot(dir, cameraForward);
			return dot > 0f; // within forward 180 degree.
		}

		private bool Intersect(in Ray ray, float maxDistance, List<RenderGeometry> renderGeometries, out RayHit rayHit)
		{
			rayHit = default;
			switch (m_Intersect)
			{
				case eIntersect.Physic:
					bool hit = Physics.Raycast(ray, out RaycastHit hitInfo, maxDistance);
					rayHit = hitInfo;
					return hit;
				case eIntersect.CustomCalculation:
					return HitColliders(ray, maxDistance, renderGeometries, out rayHit);
				default:
					return false;
			}
		}

		private bool HitColliders(in Ray ray, float maxDistance, List<RenderGeometry> geometry, out RayHit rayHit)
		{
			int cnt = geometry.Count;
			bool rayAlreadyHit = false;
			rayHit = default;
			for (int i = 0; i < cnt; i++)
			{
				bool colliderHit = false;
				if (geometry[i].m_Collider is SphereCollider sphereCollider)
					colliderHit = HitTest(ray, sphereCollider, maxDistance, ref rayHit);
				//else if (geometry[i].m_Collider is BoxCollider boxCollider)
				//	colliderHit = HitTest(ray, boxCollider, maxDistance, ref rayHit);
				//else if (geometry[i].m_Collider is CapsuleCollider capsuleCollider)
				//	colliderHit = HitTest(ray, capsuleCollider, maxDistance, ref rayHit);
				else if (geometry[i].m_Collider is MeshCollider meshCollider)
					colliderHit = HitTest(ray, meshCollider, maxDistance, ref rayHit);
				else
					throw new System.NotImplementedException();

				if (colliderHit && geometry[i].m_Material != null)
				{
					rayHit.rendergeometry = geometry[i];
					maxDistance = rayHit.distance;
					rayAlreadyHit = true;
				}
			}
			return rayAlreadyHit;
		}

		private bool HitTest(in Ray ray, SphereCollider sphereCollider, in float maxDistance, ref RayHit rayHit)
		{
			Vector3 origin = sphereCollider.transform.TransformPoint(sphereCollider.center);
			float radius = sphereCollider.radius;
			/// https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-sphere-intersection
			bool hit = Maths.IntersectRaySphere(ray, origin, radius, maxDistance, ref rayHit);
			return hit;
		}

		private bool HitTest(in Ray ray, MeshCollider meshCollider, float maxDistance, ref RayHit rayHit)
		{
			Matrix4x4 matrix = meshCollider.transform.localToWorldMatrix;
			Mesh mesh = meshCollider.sharedMesh;
			return Maths.IntersectPointOnMesh(ray, maxDistance, mesh, ref rayHit, matrix);
		}
		#endregion Collider
	}
}