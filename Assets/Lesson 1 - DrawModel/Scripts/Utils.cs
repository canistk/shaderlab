using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using Handles = UnityEditor.Handles;
using HandleUtility = UnityEditor.HandleUtility;
#endif

namespace DebugKit
{
	public static class DebugExtend
	{
		#region Line
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void DrawRay(Vector3 position, Vector3 direction, Color color = default(Color), float duration = 0, bool depthTest = false)
		{
#if UNITY_EDITOR
			if (color == default(Color))
				color = Color.white;
			Debug.DrawRay(position, direction, color, duration, depthTest);
#endif
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void DrawLine(Vector3 point1, Vector3 point2, Color color = default(Color), float duration = 0, bool depthTest = false)
		{
#if UNITY_EDITOR
			if (color == default(Color))
				color = Color.white;
			Debug.DrawLine(point1, point2, color, duration, depthTest);
#endif
		}
		/// <summary>- Debugs an arrow.</summary>
		/// <param name='position'>- The start position of the arrow.</param>
		/// <param name='direction'>- The end position of the arrow.</param>
		/// <param name='color'>- The color of the arrow.</param>
		/// <param name='duration'>- How long to draw the arrow.</param>
		/// <param name='depthTest'>- Whether or not the arrow should be faded when behind other objects.</param>
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void DrawArrow(Vector3 position, Vector3 direction, Color color = default(Color), float angle = 5f, float headLength = 0.2f, float duration = 0, bool depthTest = false)
		{
#if UNITY_EDITOR
			if (direction == Vector3.zero)
				direction = Vector3.forward;
			if (angle < 0f)
				angle = Mathf.Abs(angle);
			if (angle > 0f)
			{
				if (headLength < 0f)
					headLength = Mathf.Abs(headLength);
				float length = direction.magnitude;
				if (length < headLength)
					headLength = length;
				Vector3 headDir = direction.normalized * (-length * headLength);
				DrawCone(position + direction, headDir, color, angle, duration, depthTest);
			}

			DrawRay(position, direction, color, duration, depthTest);
#endif
		}
		#endregion Line

		#region Circle
		/// <summary>- Debugs a circle.</summary>
		/// <param name='position'>- Where the center of the circle will be positioned.</param>
		/// <param name='up'>- The direction perpendicular to the surface of the circle.</param>
		/// <param name='color'>- The color of the circle.</param>
		/// <param name='radius'>- The radius of the circle.</param>
		/// <param name='duration'>- How long to draw the circle.</param>
		/// <param name='depthTest'>- Whether or not the circle should be faded when behind other objects.</param>
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void DrawCircle(Vector3 position, Vector3 up, Color color = default(Color), float radius = 1.0f, float duration = 0, bool depthTest = false)
		{
#if UNITY_EDITOR
			up = ((up == default(Vector3)) ? Vector3.up : up).normalized * radius;
			Vector3
				forward = Vector3.Slerp(up, -up, 0.5f),
				right = Vector3.Cross(up, forward).normalized * radius;

			Matrix4x4 matrix = new Matrix4x4()
			{
				m00 = right.x,
				m10 = right.y,
				m20 = right.z,

				m01 = up.x,
				m11 = up.y,
				m21 = up.z,

				m02 = forward.x,
				m12 = forward.y,
				m22 = forward.z
			};

			Vector3
				lastPoint = position + matrix.MultiplyPoint3x4(new Vector3(Mathf.Cos(0), 0, Mathf.Sin(0))),
				nextPoint = Vector3.zero;

			for (int i = 0; i <= 90; i++)
			{
				nextPoint = position + matrix.MultiplyPoint3x4(
					new Vector3(
						Mathf.Cos((i * 4) * Mathf.Deg2Rad),
						0f,
						Mathf.Sin((i * 4) * Mathf.Deg2Rad)
						)
					);
				Debug.DrawLine(lastPoint, nextPoint, color, duration, depthTest);
				lastPoint = nextPoint;
			}
#endif
		}
		#endregion Circle
		#region Sphere
		/// <summary>- Debugs a wire sphere.</summary>
		/// <param name='position'>- The position of the center of the sphere.</param>
		/// <param name='color'>- The color of the sphere.</param>
		/// <param name='radius'>- The radius of the sphere.</param>
		/// <param name='duration'>- How long to draw the sphere.</param>
		/// <param name='depthTest'>- Whether or not the sphere should be faded when behind other objects.</param>
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void DrawWireSphere(Vector3 position, float radius = 1.0f, Color color = default(Color), float duration = 0, bool depthTest = false)
		{
#if UNITY_EDITOR
			float angle = 10.0f;

			Vector3
				x = new Vector3(position.x, position.y + radius * Mathf.Sin(0), position.z + radius * Mathf.Cos(0)),
				y = new Vector3(position.x + radius * Mathf.Cos(0), position.y, position.z + radius * Mathf.Sin(0)),
				z = new Vector3(position.x + radius * Mathf.Cos(0), position.y + radius * Mathf.Sin(0), position.z),
				new_x, new_y, new_z;

			color = (color == default(Color)) ? Color.white : color;

			for (int i = 1; i <= 36; i++)
			{
				new_x = new Vector3(position.x, position.y + radius * Mathf.Sin(angle * i * Mathf.Deg2Rad), position.z + radius * Mathf.Cos(angle * i * Mathf.Deg2Rad));
				new_y = new Vector3(position.x + radius * Mathf.Cos(angle * i * Mathf.Deg2Rad), position.y, position.z + radius * Mathf.Sin(angle * i * Mathf.Deg2Rad));
				new_z = new Vector3(position.x + radius * Mathf.Cos(angle * i * Mathf.Deg2Rad), position.y + radius * Mathf.Sin(angle * i * Mathf.Deg2Rad), position.z);

				Debug.DrawLine(x, new_x, color, duration, depthTest);
				Debug.DrawLine(y, new_y, color, duration, depthTest);
				Debug.DrawLine(z, new_z, color, duration, depthTest);

				x = new_x;
				y = new_y;
				z = new_z;
			}
#endif
		}
		#endregion Sphere

		/// <summary>- Debugs a cone.</summary>
		/// <param name='position'>- The position for the tip of the cone.</param>
		/// <param name='direction'>- The direction for the cone gets wider in.</param>
		/// <param name='angle'>- The angle of the cone.</param>
		/// <param name='color'>- The color of the cone.</param>
		/// <param name='duration'>- How long to draw the cone.</param>
		/// <param name='depthTest'>- Whether or not the cone should be faded when behind other objects.</param>
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void DrawCone(Vector3 position, Vector3 direction, Color color = default(Color), float angle = 45f, float duration = 0, bool depthTest = false, bool flip = false)
		{
#if UNITY_EDITOR
			if (flip)
			{
				position += direction;
				direction *= -1f;
			}

			float length = direction.magnitude;
			angle = Mathf.Clamp(angle, 0f, 90f);

			Vector3
				forward = direction,
				up = Vector3.Slerp(forward, -forward, 0.5f),
				right = Vector3.Cross(forward, up).normalized * length,

				slerpedVector = Vector3.Slerp(forward, up, angle / 90.0f);

			Plane farPlane = new Plane(-direction, position + forward);
			Ray distRay = new Ray(position, slerpedVector);

			float dist;
			farPlane.Raycast(distRay, out dist);

			Debug.DrawRay(position, slerpedVector.normalized * dist, color, duration, depthTest);
			Debug.DrawRay(position, Vector3.Slerp(forward, -up, angle / 90.0f).normalized * dist, color, duration, depthTest);
			Debug.DrawRay(position, Vector3.Slerp(forward, right, angle / 90.0f).normalized * dist, color, duration, depthTest);
			Debug.DrawRay(position, Vector3.Slerp(forward, -right, angle / 90.0f).normalized * dist, color, duration, depthTest);

			DrawCircle(position + forward, direction, color, (forward - (slerpedVector.normalized * dist)).magnitude, duration, depthTest);
			DrawCircle(position + (forward * 0.5f), direction, color, ((forward * 0.5f) - (slerpedVector.normalized * (dist * 0.5f))).magnitude, duration, depthTest);
#endif
		}
	}

	public static class GizmosExtend
	{
		public static void DrawRay(Vector3 position, Vector3 direction, Color color = default(Color))
		{
#if UNITY_EDITOR
			if (color == default)
				Gizmos.DrawRay(position, direction);
			else
			{
				using (new ColorScope(color))
				{
					Gizmos.DrawRay(position, direction);
				}
			}
#endif
		}
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void DrawLabel(Vector3 position, string text, float offsetX = 0f, float offsetY = 0f, bool background = true)
		{
#if UNITY_EDITOR
			if (IsHandleHackAvailable)
			{
				Transform cam = UnityEditor.SceneView.currentDrawingSceneView != null ?
					UnityEditor.SceneView.currentDrawingSceneView.camera.transform : // Scene View
					Camera.main.transform; // Only Game View
				if (Vector3.Dot(cam.forward, position - cam.position) > 0)
				{
					Vector3 pos = position;
					if (offsetX != 0f || offsetY != 0f)
					{
						Vector3 camRightVector = cam.right * offsetX; // base on view
						pos += camRightVector + new Vector3(0f, offsetY, 0f); // base on target
					}

					if (background)
						Handles.Label(pos, text, GUI.skin.textArea);
					else
						Handles.Label(pos, text);
				}
			}
#endif
		}

#if UNITY_EDITOR
		private static bool IsHandleHackAvailable =>
			UnityEditor.SceneView.currentDrawingSceneView != null ||
			(Application.isPlaying && Camera.main != null);
#else
		private const bool IsHandleHackAvailable = false;
#endif
	}
	public struct ColorScope : System.IDisposable
	{
		Color oldColor;
		public ColorScope(Color color)
		{
			oldColor = Gizmos.color;
			Gizmos.color = color == default(Color) ? oldColor : color;
		}

		public void Dispose()
		{
			Gizmos.color = oldColor;
		}
	}

}